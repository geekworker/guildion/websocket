package constants

type MessageEvent string

const (
    Join       = MessageEvent("Join")
    Leave      = MessageEvent("Leave")
    CheckFirst = MessageEvent("CheckFirst")
    Switch     = MessageEvent("Switch")
    KeepSwitch = MessageEvent("KeepSwitch")
)

func (v MessageEvent) String() string {
    return string(v)
}
