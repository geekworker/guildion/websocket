package entity

import (
  // "time"

  "github.com/jinzhu/gorm"
)

type MessageEntity struct {
  ID        int `gorm:"col:id"`
  MemberID  int `gorm:"col:member_id"`
  Data      string `gorm:"col:data"`
  // MARK: sql: Scan error on column index 2, name "created_at": unsupported Scan, storing driver.Value type []uint8 into type *time.Time
  // CreatedAt time.Time `gorm:"col:created_at"`
}

func (message MessageEntity) TableName() string {
  return "messages"
}

func (message MessageEntity) Validate(db *gorm.DB) {
}
