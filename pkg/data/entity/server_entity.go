package entity

import (
  // "time"

  "github.com/jinzhu/gorm"
)

type ServerEntity struct {
  ID            int    `gorm:"col:id"`
  Host          string `gorm:"col:host"`
  // MARK: sql: Scan error on column index 2, name "created_at": unsupported Scan, storing driver.Value type []uint8 into type *time.Time
  // CreatedAt       time.Time `gorm:"col:created_at"`
}

func (server ServerEntity) TableName() string {
  return "servers"
}

func (server ServerEntity) Validate(db *gorm.DB) {
}
