package entity

import (
  // "time"

  "github.com/jinzhu/gorm"
)

type ClientEntity struct {
  ID              int    `gorm:"col:id"`
  ServerID        int    `gorm:"col:server_id"`
  ConnectionID    string `gorm:"col:connectoin_id"`
  SecWebsocketKey string `gorm:"col:sec_websocket_key"`
  Connecting      bool   `gorm:"col:connecting"`
  // MARK: sql: Scan error on column index 2, name "created_at": unsupported Scan, storing driver.Value type []uint8 into type *time.Time
  // CreatedAt       time.Time `gorm:"col:created_at"`
}

func (client ClientEntity) TableName() string {
  return "clients"
}

func (client ClientEntity) Validate(db *gorm.DB) {
}
