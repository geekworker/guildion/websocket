package entity

import (
  // "time"

  "github.com/jinzhu/gorm"
)

type MemberEntity struct {
  ID        int `gorm:"col:id"`
  ClientID  int `gorm:"col:client_id"`
  RoomID    int `gorm:"col:room_id"`
  // MARK: sql: Scan error on column index 2, name "created_at": unsupported Scan, storing driver.Value type []uint8 into type *time.Time
  // CreatedAt time.Time `gorm:"col:created_at"`
}

func (member MemberEntity) TableName() string {
  return "members"
}

func (member MemberEntity) Validate(db *gorm.DB) {
}
