package entity

import (
  // "time"
  "strings"

  "github.com/jinzhu/gorm"

  "github.com/Zakimiii/guildion-signaling/pkg/constants"
)

type RoomEntity struct {
  ID        int    `gorm:"col:id"`
  Name      string `gorm:"col:name"`
  // MARK: sql: Scan error on column index 2, name "created_at": unsupported Scan, storing driver.Value type []uint8 into type *time.Time
  // CreatedAt time.Time `gorm:"col:created_at"`
}

func (room RoomEntity) TableName() string {
  return "rooms"
}

func (room RoomEntity) Validate(db *gorm.DB) {
}

func (room RoomEntity) GetParentName() string {
  slice := strings.Split(room.Name, constants.ROOM_PATH_SEPARATOR)
  if len(slice) == 0 {
    return ""
  }
  return slice[0]
}
