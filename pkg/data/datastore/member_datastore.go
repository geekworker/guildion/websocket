package datastore

import (
  "errors"

  "github.com/Zakimiii/guildion-signaling/pkg/external/mysql"
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type MemberDataStore struct {
  network.Singleton
}

var memberDataStore *MemberDataStore

func GetMemberDataStore() *MemberDataStore {
  if memberDataStore == nil {
    memberDataStore = &MemberDataStore{}
  }
  return memberDataStore
}

func (datastore *MemberDataStore) GetRoomMembers(room *entity.RoomEntity) ([]entity.MemberEntity, error) {
  var members []entity.MemberEntity
  mysql.DB.Where("room_id = ?", room.ID).Find(&members)
  return members, nil
}

func (datastore *MemberDataStore) Create(member *entity.MemberEntity) error {
  if err := mysql.DB.Create(member).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *MemberDataStore) Update(member *entity.MemberEntity) error {
  if member.ID == 0 { return errors.New("") }
  if err := mysql.DB.Save(member).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *MemberDataStore) Save(member *entity.MemberEntity) error {
  if err := mysql.DB.Save(member).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *MemberDataStore) FirstOrCreate(member *entity.MemberEntity) error {
  if err := mysql.DB.Where(entity.MemberEntity{ ClientID: member.ClientID, RoomID: member.RoomID }).FirstOrCreate(member).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *MemberDataStore) Delete(member *entity.MemberEntity) error {
  if err := mysql.DB.Where("id = ?", member.ID).Delete(member).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *MemberDataStore) Leave(client_id int, room_id int) error {
  var member = entity.MemberEntity{
    ClientID: client_id,
    RoomID: room_id,
  }
  if err := mysql.DB.Where("client_id = ? AND room_id = ?", client_id, room_id).Delete(&member).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *MemberDataStore) Disconnect(client_id int) error {
  var member = entity.MemberEntity{
    ClientID: client_id,
  }
  if err := mysql.DB.Where("client_id = ?", client_id).Delete(&member).Error; err != nil {
    return err
  }
  return nil
}
