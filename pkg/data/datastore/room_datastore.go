package datastore

import (
  "errors"

  "github.com/Zakimiii/guildion-signaling/pkg/external/mysql"
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type RoomDataStore struct {
  network.Singleton
}

var roomDataStore *RoomDataStore

func GetRoomDataStore() *RoomDataStore {
  if roomDataStore == nil {
    roomDataStore = &RoomDataStore{}
  }
  return roomDataStore
}

func (datastore *RoomDataStore) GetEntity(room *entity.RoomEntity) error {
  if err := mysql.DB.Where(room).Find(room).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *RoomDataStore) GetParentEntity(room *entity.RoomEntity) (*entity.RoomEntity, error) {
  roomname := room.GetParentName()
  if roomname == "" {
    return nil, nil
  }
  var parent = entity.RoomEntity{
    Name: roomname,
  }
  if err := mysql.DB.Where(parent).Find(&parent).Error; err != nil {
    return nil, err
  }
  return &parent, nil
}

func (datastore *RoomDataStore) Create(room *entity.RoomEntity) error {
  if err := mysql.DB.Create(room).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *RoomDataStore) Update(room *entity.RoomEntity) error {
  if room.ID == 0 { return errors.New("") }
  if err := mysql.DB.Save(room).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *RoomDataStore) Save(room *entity.RoomEntity) error {
  if err := mysql.DB.Save(room).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *RoomDataStore) FirstOrCreate(room *entity.RoomEntity) error {
  if err := mysql.DB.Where(entity.RoomEntity{ Name: room.Name }).FirstOrCreate(room).Error; err != nil {
    return err
  }
  return nil
}
