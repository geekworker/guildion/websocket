package datastore

import (
  "errors"

  "github.com/thoas/go-funk"

  "github.com/Zakimiii/guildion-signaling/pkg/external/mysql"
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type ClientDataStore struct {
  network.Singleton
}

var clientDataStore *ClientDataStore

func GetClientDataStore() *ClientDataStore {
  if clientDataStore == nil {
    clientDataStore = &ClientDataStore{}
  }
  return clientDataStore
}

func (datastore *ClientDataStore) GetMemberClients(client *entity.ClientEntity) ([]entity.ClientEntity, error) {
  var members []entity.MemberEntity
  mysql.DB.Where("client_id = ?", client.ID).Find(&members)
  var rooms []entity.RoomEntity
  var room_ids []int = funk.Map(members, func(member entity.MemberEntity) int { return member.RoomID }).([]int)
  if len(room_ids) == 0 { return make([]entity.ClientEntity, 0), nil }
  mysql.DB.Where("id IN ?", room_ids).Find(&rooms)
  var clients []entity.ClientEntity
  for i := 0; i < len(rooms); i++ {
    var r_members []entity.MemberEntity
    mysql.DB.Where("room_id = ?", rooms[i].ID).Find(&r_members)
    var r_clients []entity.ClientEntity
    var client_ids []int = funk.Map(r_members, func(member entity.MemberEntity) int { return member.ClientID }).([]int)
    if len(client_ids) == 0 { return make([]entity.ClientEntity, 0), nil }
    mysql.DB.Where("id = IN", client_ids).Find(&r_clients)
    clients = append(clients, r_clients...)
  }
  return clients, nil
}

func (datastore *ClientDataStore) Create(client *entity.ClientEntity) error {
  client.ServerID = network.GetCurrentServer().ID
  if err := mysql.DB.Create(client).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *ClientDataStore) Update(client *entity.ClientEntity) error {
  client.ServerID = network.GetCurrentServer().ID
  if client.ID == 0 { return errors.New("") }
  if err := mysql.DB.Save(client).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *ClientDataStore) Save(client *entity.ClientEntity) error {
  client.ServerID = network.GetCurrentServer().ID
  if err := mysql.DB.Save(client).Error; err != nil {
    return err
  }
  return nil
}
