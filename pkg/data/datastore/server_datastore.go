package datastore

import (
  "errors"

  "github.com/Zakimiii/guildion-signaling/pkg/external/mysql"
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type ServerDataStore struct {
  network.Singleton
}

var serverDataStore *ServerDataStore

func GetServerDataStore() *ServerDataStore {
  if serverDataStore == nil {
    serverDataStore = &ServerDataStore{}
  }
  return serverDataStore
}

func (datastore *ServerDataStore) GetEntity(server *entity.ServerEntity) error {
  if err := mysql.DB.Where(server).Find(server).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *ServerDataStore) Create(server *entity.ServerEntity) error {
  if err := mysql.DB.Create(server).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *ServerDataStore) Update(server *entity.ServerEntity) error {
  if server.ID == 0 { return errors.New("") }
  if err := mysql.DB.Save(server).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *ServerDataStore) Save(server *entity.ServerEntity) error {
  if err := mysql.DB.Save(server).Error; err != nil {
    return err
  }
  return nil
}

func (datastore *ServerDataStore) FirstOrCreate(server *entity.ServerEntity) error {
  if err := mysql.DB.Where(entity.ServerEntity{ Host: server.Host }).FirstOrCreate(server).Error; err != nil {
    return err
  }
  return nil
}
