package datastore

import (
  // "github.com/Zakimiii/guildion-signaling/pkg/external/mysql"
  // "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type MessageDataStore struct {
  network.Singleton
}

var messageDataStore *MessageDataStore

func GetMessageDataStore() *MessageDataStore {
  if messageDataStore == nil {
    messageDataStore = &MessageDataStore{}
  }
  return messageDataStore
}
