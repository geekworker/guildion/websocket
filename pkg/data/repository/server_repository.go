package repository

import (
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
)

type ServerRepository interface {
  GetEntity(server *entity.ServerEntity) error
  Create(server *entity.ServerEntity) error
  Update(server *entity.ServerEntity) error
  Save(server *entity.ServerEntity) error
  FirstOrCreate(server *entity.ServerEntity) error
}
