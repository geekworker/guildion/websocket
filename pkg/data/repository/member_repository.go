package repository

import (
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
)

type MemberRepository interface {
  GetRoomMembers(room *entity.RoomEntity) ([]entity.MemberEntity, error)
  Create(member *entity.MemberEntity) error
  Update(member *entity.MemberEntity) error
  Save(member *entity.MemberEntity) error
  FirstOrCreate(member *entity.MemberEntity) error
  Delete(member *entity.MemberEntity) error
  Leave(client_id int, room_id int) error
  Disconnect(client_id int) error
}
