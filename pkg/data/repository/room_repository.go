package repository

import (
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
)

type RoomRepository interface {
  GetEntity(room *entity.RoomEntity) error
  GetParentEntity(room *entity.RoomEntity) (*entity.RoomEntity, error)
  Create(room *entity.RoomEntity) error
  Update(room *entity.RoomEntity) error
  Save(room *entity.RoomEntity) error
  FirstOrCreate(room *entity.RoomEntity) error
}
