package repository

import (
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
)

type ClientRepository interface {
  GetMemberClients(client *entity.ClientEntity) ([]entity.ClientEntity, error)
  Create(client *entity.ClientEntity) error
  Update(client *entity.ClientEntity) error
  Save(client *entity.ClientEntity) error
}
