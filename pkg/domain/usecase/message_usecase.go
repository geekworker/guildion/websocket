package usecase

import (
  "log"

  "github.com/Zakimiii/guildion-signaling/pkg/network"
  // "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/data/repository"
  "github.com/Zakimiii/guildion-signaling/pkg/data/datastore"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
  // "github.com/Zakimiii/guildion-signaling/pkg/domain/translator"
)

type MessageUseCase struct {
  network.Singleton
  ClientRepository repository.ClientRepository
  RoomRepository repository.RoomRepository
  ServerRepository repository.ServerRepository
  MemberRepository repository.MemberRepository
  MessageRepository repository.MessageRepository
}

var messageUseCase *MessageUseCase

func GetMessageUseCase() *MessageUseCase {
  if messageUseCase == nil {
    messageUseCase = &MessageUseCase{
      ClientRepository: datastore.GetClientDataStore(),
      RoomRepository: datastore.GetRoomDataStore(),
      ServerRepository: datastore.GetServerDataStore(),
      MemberRepository: datastore.GetMemberDataStore(),
      MessageRepository: datastore.GetMessageDataStore(),
    }
  }
  return messageUseCase
}

func (usecase *MessageUseCase) Send(clients []*model.ClientModel, cmsg model.ClientMsgModel, current_client *model.ClientModel) error {
  for _, client := range clients {
    if current_client.ID != client.ID {
      client.Send <- cmsg.Message
    }
  }
  log.Println("debug: SENT TO --", len(clients) ,"clients")
  return nil
}
