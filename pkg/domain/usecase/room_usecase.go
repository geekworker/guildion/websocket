package usecase

import (
  "github.com/Zakimiii/guildion-signaling/pkg/network"
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/data/repository"
  "github.com/Zakimiii/guildion-signaling/pkg/data/datastore"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/translator"
)

type RoomUseCase struct {
  network.Singleton
  ClientRepository repository.ClientRepository
  RoomRepository repository.RoomRepository
  ServerRepository repository.ServerRepository
  MemberRepository repository.MemberRepository
  MessageRepository repository.MessageRepository
}

var roomUseCase *RoomUseCase

func GetRoomUseCase() *RoomUseCase {
  if roomUseCase == nil {
    roomUseCase = &RoomUseCase{
      ClientRepository: datastore.GetClientDataStore(),
      RoomRepository: datastore.GetRoomDataStore(),
      ServerRepository: datastore.GetServerDataStore(),
      MemberRepository: datastore.GetMemberDataStore(),
      MessageRepository: datastore.GetMessageDataStore(),
    }
  }
  return roomUseCase
}

func (usecase *RoomUseCase) GetModel(room *model.RoomModel) error {
  r := translator.RoomModel_RoomEntityTranslate(room)
  if err := usecase.RoomRepository.GetEntity(r); err != nil {
    return err
  }
  room.ID = r.ID
  return nil
}

func (usecase *RoomUseCase) GetParentModel(room *model.RoomModel) (*model.RoomModel, error) {
  r := translator.RoomModel_RoomEntityTranslate(room)
  parent, err := usecase.RoomRepository.GetParentEntity(r)
  if err != nil {
    return nil, err
  }
  var result = &model.RoomModel{
    ID: parent.ID,
    Name: parent.Name,
  }
  return result, nil
}

func (usecase *RoomUseCase) Join(client *model.ClientModel, room *model.RoomModel) error {
  r := translator.RoomModel_RoomEntityTranslate(room)
  if err := usecase.RoomRepository.FirstOrCreate(r); err != nil {
    return err
  }
  room.ID = r.ID
  var member = &entity.MemberEntity{
    ClientID: client.ID,
    RoomID: r.ID,
  }
  if err := usecase.MemberRepository.FirstOrCreate(member); err != nil {
    return err
  }
  return nil
}

func (usecase *RoomUseCase) Leave(client *model.ClientModel, room *model.RoomModel) error {
  if err := usecase.MemberRepository.Leave(client.ID, room.ID); err != nil {
    return err
  }
  return nil
}
