package usecase

import (
  "os"
  "log"

  "github.com/Zakimiii/guildion-signaling/pkg/network"
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/data/repository"
  "github.com/Zakimiii/guildion-signaling/pkg/data/datastore"
)

type ServerUseCase struct {
  network.Singleton
  ClientRepository repository.ClientRepository
  RoomRepository repository.RoomRepository
  ServerRepository repository.ServerRepository
  MemberRepository repository.MemberRepository
  MessageRepository repository.MessageRepository
}

var serverUseCase *ServerUseCase

func GetServerUseCase() *ServerUseCase {
  if serverUseCase == nil {
    serverUseCase = &ServerUseCase{
      ClientRepository: datastore.GetClientDataStore(),
      RoomRepository: datastore.GetRoomDataStore(),
      ServerRepository: datastore.GetServerDataStore(),
      MemberRepository: datastore.GetMemberDataStore(),
      MessageRepository: datastore.GetMessageDataStore(),
    }
  }
  return serverUseCase
}

func (usecase *ServerUseCase) InitializeCurrentServer() (*entity.ServerEntity, error) {
  host := os.Getenv("PUBLIC_IP")
  if host == "" {
    log.Fatal("$PUBLIC_IP must be set")
  }
  var server = &entity.ServerEntity{
    Host: host,
  }
  if err := usecase.ServerRepository.FirstOrCreate(server); err != nil {
    return nil, err
  }
  log.Println("info: * Server bootstrap on", host)
  return server, nil
}
