package usecase

import (
  "github.com/Zakimiii/guildion-signaling/pkg/network"
  // "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/data/repository"
  "github.com/Zakimiii/guildion-signaling/pkg/data/datastore"
  // "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
  // "github.com/Zakimiii/guildion-signaling/pkg/domain/translator"
)

type MemberUseCase struct {
  network.Singleton
  ClientRepository repository.ClientRepository
  RoomRepository repository.RoomRepository
  ServerRepository repository.ServerRepository
  MemberRepository repository.MemberRepository
  MessageRepository repository.MessageRepository
}

var memberUseCase *MemberUseCase

func GetMemberUseCase() *MemberUseCase {
  if memberUseCase == nil {
    memberUseCase = &MemberUseCase{
      ClientRepository: datastore.GetClientDataStore(),
      RoomRepository: datastore.GetRoomDataStore(),
      ServerRepository: datastore.GetServerDataStore(),
      MemberRepository: datastore.GetMemberDataStore(),
      MessageRepository: datastore.GetMessageDataStore(),
    }
  }
  return memberUseCase
}
