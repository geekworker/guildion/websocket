package usecase

import (
  "github.com/Zakimiii/guildion-signaling/pkg/network"
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/data/repository"
  "github.com/Zakimiii/guildion-signaling/pkg/data/datastore"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/translator"
)

type ClientUseCase struct {
  network.Singleton
  ClientRepository repository.ClientRepository
  RoomRepository repository.RoomRepository
  ServerRepository repository.ServerRepository
  MemberRepository repository.MemberRepository
  MessageRepository repository.MessageRepository
}

var clientUseCase *ClientUseCase

func GetClientUseCase() *ClientUseCase {
  if clientUseCase == nil {
    clientUseCase = &ClientUseCase{
      ClientRepository: datastore.GetClientDataStore(),
      RoomRepository: datastore.GetRoomDataStore(),
      ServerRepository: datastore.GetServerDataStore(),
      MemberRepository: datastore.GetMemberDataStore(),
      MessageRepository: datastore.GetMessageDataStore(),
    }
  }
  return clientUseCase
}

func (usecase *ClientUseCase) Connect(client *model.ClientModel) error {
  client.Connecting = true
  network.AppendClients(client)
  entity := translator.ClientModel_ClientEntityTranslate(client)
  if err := usecase.ClientRepository.Create(entity); err != nil {
    return err
  }
  client.ID = entity.ID
  return nil
}

func (usecase *ClientUseCase) Disconnect(client *model.ClientModel) error {
  client.Connecting = false
  network.RemoveClients()
  entity := translator.ClientModel_ClientEntityTranslate(client)
  if err := usecase.ClientRepository.Update(entity); err != nil {
    return err
  }
  if err := usecase.MemberRepository.Disconnect(entity.ID); err != nil {
    return err
  }
  return nil
}

func (usecase *ClientUseCase) GetMemberClients(client *model.ClientModel) ([]entity.ClientEntity, error) {
  entity := translator.ClientModel_ClientEntityTranslate(client)
  clients, err := usecase.ClientRepository.GetMemberClients(entity)
  return clients, err
}

func (usecase *ClientUseCase) GetRoomClients(room *model.RoomModel) ([]*model.ClientModel, error) {
  r := translator.RoomModel_RoomEntityTranslate(room)
  members, err := usecase.MemberRepository.GetRoomMembers(r)
  var clients = make([]*model.ClientModel, 0)
  var c_clients = network.GetClients()
  for _, member := range members {
    for _, c_client := range c_clients {
      if c_client.ID == member.ClientID {
        clients = append(clients, c_client)
      }
    }
  }
  return clients, err
}
