package translator

import (
  "log"
  "encoding/json"

  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
)

func ClientMsgModel_MessageModelTranslate(input model.ClientMsgModel) model.MessageModel {
  var output model.MessageModel
  if err := json.Unmarshal(input.Message, &output); err != nil {
    log.Println(err)
  }
  return output
}
