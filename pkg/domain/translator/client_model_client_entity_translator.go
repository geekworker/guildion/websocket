package translator

import (
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
)

func ClientModel_ClientEntityTranslate(input *model.ClientModel) *entity.ClientEntity {
  output := &entity.ClientEntity{
    ID: input.ID,
    ConnectionID: input.ConnectionID,
    SecWebsocketKey: input.SecWebsocketKey,
    Connecting: input.Connecting,
  }
  return output
}
