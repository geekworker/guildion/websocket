package translator

import (
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
)

func RoomModel_RoomEntityTranslate(input *model.RoomModel) *entity.RoomEntity {
  output := &entity.RoomEntity{
    ID: input.ID,
    Name: input.Name,
  }
  return output
}
