package model

type MessageModel struct {
  Type     string `json:"type"`
  Roomname string `json:"roomname"`
  Id       string `json:"id"`
  Parent   bool   `json:"parent,string"`
  Children bool   `json:"children,string"`
}
