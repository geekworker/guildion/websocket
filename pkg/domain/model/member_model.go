package model

type MemberModel struct {
  ID       int
  ClientID int
  RoomID   int
}
