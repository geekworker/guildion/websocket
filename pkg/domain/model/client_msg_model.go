package model

type ClientMsgModel struct {
  Client  *ClientModel
  Message []byte
}
