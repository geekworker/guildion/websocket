package model

import (
  "net/http"

  "github.com/gorilla/websocket"
  "github.com/google/uuid"

  "github.com/Zakimiii/guildion-signaling/pkg/constants"
)

type ClientModel struct {
  ID              int
  ConnectionID    string
  SecWebsocketKey string
  Connecting      bool

  Socket          *websocket.Conn
  Send            chan []byte
}

func GenerateNewClient(w http.ResponseWriter, req *http.Request, Socket *websocket.Conn) *ClientModel {
  uuidObj, _ := uuid.NewUUID()
  return &ClientModel{
    ConnectionID: uuidObj.String(),
    SecWebsocketKey: req.Header.Get("Sec-Websocket-Key"),

    Socket: Socket,
    Send: make(chan []byte, constants.MessageBufferSize),
  }
}
