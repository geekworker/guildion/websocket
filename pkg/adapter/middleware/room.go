package middleware

import (
  "log"
  "net/http"

  "github.com/Zakimiii/guildion-signaling/pkg/adapter/controllers"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

/*
* PATH: GET(/api/v1/room/{name}/members/count)
*/
func RoomMembersCountHandler(w http.ResponseWriter, req *http.Request) {
  if !IsValidRequest(w, req) { return }
  log.Println("debug: " + "[" + req.Method + "] " + "<--", req.URL.Path)
  logger := network.RequestLogger{
    Action:  req.Method,
    Path: "--> " + req.URL.Path,
  }
  logger.Start()
  roomController := controllers.GetRoomController()
  roomController.GetRoomMembersCount(w, req)
  logger.End()
}
