package middleware

import (
  "github.com/Zakimiii/guildion-signaling/pkg/adapter/controllers"
)

/*
* PATH: CONSTRUCTOR
*/
func ScaleConstructHandler() {
  controller := controllers.GetServerController()
  controller.InitializeServer()
}

/*
* PATH: DESTRUCTOR
*/
func ScaleDestructHandler() {
}
