package middleware

import (
  "net/http"

  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

func IsValidRequest(w http.ResponseWriter, req *http.Request) bool {
  env := network.GetENV()
  key := req.Header.Get("API_KEY")
  secret := req.Header.Get("API_SECRET")
  result := key == env.SIGNALING_KEY && secret == env.SIGNALING_SECRET
  if !result {
    http.Error(w, "", http.StatusBadRequest)
  }
  return result
}
