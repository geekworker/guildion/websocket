package middleware

import (
  "net/http"

  "github.com/Zakimiii/guildion-signaling/pkg/adapter/controllers"
)

/*
* PATH: ROOT(/signaling/v1/)
*/
func SignalingConnectHandler(w http.ResponseWriter, req *http.Request) {
  if !IsValidRequest(w, req) { return }
  signalingController := controllers.GetSignalingController()
  signalingController.Connect(w, req)
}
