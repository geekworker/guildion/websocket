package controllers

import (
  "net/http"
  "encoding/json"

  "github.com/gorilla/mux"

  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/usecase"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type RoomController struct {
  network.Singleton
  ClientUseCase *usecase.ClientUseCase
  RoomUseCase *usecase.RoomUseCase
  ServerUseCase *usecase.ServerUseCase
  MemberUseCase *usecase.MemberUseCase
  MessageUseCase *usecase.MessageUseCase
}

var roomController *RoomController

func GetRoomController() *RoomController {
  if roomController == nil {
    roomController = &RoomController{
      ClientUseCase: usecase.GetClientUseCase(),
      RoomUseCase: usecase.GetRoomUseCase(),
      ServerUseCase: usecase.GetServerUseCase(),
      MemberUseCase: usecase.GetMemberUseCase(),
      MessageUseCase: usecase.GetMessageUseCase(),
    }
  }
  return roomController
}

type GetRoomMembersCountPayload struct {
    Count int `json:"count"`
}

func (controller *RoomController) GetRoomMembersCount(w http.ResponseWriter, req *http.Request) {
  w.Header().Set("Content-Type", "application/json")
  vars := mux.Vars(req)
  name := vars["name"]
  var room = &model.RoomModel{
    Name: name,
  }
  if err := controller.RoomUseCase.GetModel(room); err != nil {
    var payload = &GetRoomMembersCountPayload{
      Count: 0,
    }
    json.NewEncoder(w).Encode(payload)
    return
  }
  clients, _ := controller.ClientUseCase.GetRoomClients(room)
  var payload = &GetRoomMembersCountPayload{
    Count: len(clients),
  }
  json.NewEncoder(w).Encode(payload)
}
