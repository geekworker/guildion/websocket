package controllers

import (
  "log"
  "net/http"

  "github.com/gorilla/websocket"

  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/translator"
  "github.com/Zakimiii/guildion-signaling/pkg/domain/usecase"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
  "github.com/Zakimiii/guildion-signaling/pkg/constants"
)

type SignalingController struct {
  network.Singleton
  ClientUseCase *usecase.ClientUseCase
  RoomUseCase *usecase.RoomUseCase
  ServerUseCase *usecase.ServerUseCase
  MemberUseCase *usecase.MemberUseCase
  MessageUseCase *usecase.MessageUseCase
}

var signalingController *SignalingController

func GetSignalingController() *SignalingController {
  if signalingController == nil {
    signalingController = &SignalingController{
      ClientUseCase: usecase.GetClientUseCase(),
      RoomUseCase: usecase.GetRoomUseCase(),
      ServerUseCase: usecase.GetServerUseCase(),
      MemberUseCase: usecase.GetMemberUseCase(),
      MessageUseCase: usecase.GetMessageUseCase(),
    }
  }
  return signalingController
}

func (controller *SignalingController) Connect(w http.ResponseWriter, req *http.Request) {
  Socket, err := constants.Upgrader.Upgrade(w, req, nil)
  if err != nil {
    log.Println("ServeHTTP:", err)
    http.Error(w, "", http.StatusBadRequest)
    return
  }
  client := model.GenerateNewClient(w, req, Socket)
  log.Println("debug: [CONNECTED] <--", client.ConnectionID)
  if u_err := controller.ClientUseCase.Connect(client); u_err != nil {
    http.Error(w, "", http.StatusBadRequest)
    return
  }

  defer func() {
    controller.Disconnect(client)
  }()

  go controller.Write(client)
  controller.Read(client)
}

func (controller *SignalingController) Read(client *model.ClientModel) {
  defer func() {
    client.Socket.Close()
  }()
  for {
    _, message, err := client.Socket.ReadMessage()
    if err != nil {
      return
    }
    cmsg := model.ClientMsgModel{
      Client: client,
      Message: message,
    }
    // MARK: Handler Forward by function/channels
    controller.Forward(client, cmsg)
  }
}

func (controller *SignalingController) Write(client *model.ClientModel) {
  defer func() {
    client.Socket.Close()
  }()
  for msg := range client.Send {
    err := client.Socket.WriteMessage(websocket.TextMessage, msg)
    if err != nil {
      return
    }
  }
}

func (controller *SignalingController) Forward(client *model.ClientModel, cmsg model.ClientMsgModel) {
  message := translator.ClientMsgModel_MessageModelTranslate(cmsg)
  logger := network.RequestLogger{
    Action:  message.Type,
    Path: "--> " + "/" + message.Roomname,
  }
  logger.Start()

  switch message.Type {
    case constants.Join.String(): controller.forwardForJoin(client, message)
    case constants.Leave.String(): controller.forwardForLeave(client, message)
    case constants.CheckFirst.String(): controller.forwardForCheckFirst(client, message, cmsg)
    default: controller.forwardForMembers(client, message, cmsg)
  }

  logger.End()
}

func (controller *SignalingController) forwardForJoin(client *model.ClientModel, message model.MessageModel) {
  var room = &model.RoomModel{
    Name: message.Roomname,
  }
  if err := controller.RoomUseCase.Join(client, room); err != nil {
    return
  }
}

func (controller *SignalingController) forwardForLeave(client *model.ClientModel, message model.MessageModel) {
  var room = &model.RoomModel{
    Name: message.Roomname,
  }
  if err := controller.RoomUseCase.GetModel(room); err != nil {
    return
  }
  if room.ID == 0 {
    return
  }
  if err := controller.RoomUseCase.Leave(client, room); err != nil {
    return
  }
}

func (controller *SignalingController) forwardForCheckFirst(client *model.ClientModel, message model.MessageModel, cmsg model.ClientMsgModel) {
  var room = &model.RoomModel{
    Name: message.Roomname,
  }
  if err := controller.RoomUseCase.GetModel(room); err != nil {
    return
  }
  if room.ID == 0 {
    return
  }
  clients, _ := controller.ClientUseCase.GetRoomClients(room)
  if len(clients) > 0 {
    if clients[0] == client {
      client.Send <- cmsg.Message
    }
  }
}

func (controller *SignalingController) forwardForMembers(client *model.ClientModel, message model.MessageModel, cmsg model.ClientMsgModel) {
  var room = &model.RoomModel{
    Name: message.Roomname,
  }
  if err := controller.RoomUseCase.GetModel(room); err != nil {
    return
  }
  if room.ID == 0 {
    return
  }
  clients, _ := controller.ClientUseCase.GetRoomClients(room)
  log.Println("debug: PROCESSING in ROOM#" + room.Name)
  if err := controller.MessageUseCase.Send(clients, cmsg, client); err != nil {
    return
  }

  if !message.Parent {
    return
  }
  parent, _err := controller.RoomUseCase.GetParentModel(room)
  log.Println("debug: PROCESSING in ROOM#" + parent.Name)
  if _err != nil {
    return
  }
  p_clients, _ := controller.ClientUseCase.GetRoomClients(parent)
  if err := controller.MessageUseCase.Send(p_clients, cmsg, client); err != nil {
    return
  }
}

func (controller *SignalingController) Disconnect(client *model.ClientModel) {
  log.Println("debug: [DISCONNECTED] <--", client.ConnectionID)
  controller.ClientUseCase.Disconnect(client)
}
