package controllers

import (
  "github.com/Zakimiii/guildion-signaling/pkg/domain/usecase"
  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type ServerController struct {
  network.Singleton
  ClientUseCase *usecase.ClientUseCase
  RoomUseCase *usecase.RoomUseCase
  ServerUseCase *usecase.ServerUseCase
  MemberUseCase *usecase.MemberUseCase
  MessageUseCase *usecase.MessageUseCase
}

var serverController *ServerController

func GetServerController() *ServerController {
  if serverController == nil {
    serverController = &ServerController{
      ClientUseCase: usecase.GetClientUseCase(),
      RoomUseCase: usecase.GetRoomUseCase(),
      ServerUseCase: usecase.GetServerUseCase(),
      MemberUseCase: usecase.GetMemberUseCase(),
      MessageUseCase: usecase.GetMessageUseCase(),
    }
  }
  return serverController
}

func (controller *ServerController) InitializeServer() {
  server, err := controller.ServerUseCase.InitializeCurrentServer()
  if err != nil {
    return
  }
  current_server := network.GetCurrentServer()
  current_server.ID = server.ID
  current_server.Host = server.Host
}
