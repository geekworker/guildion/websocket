package network

import (
	"os"

	"github.com/Zakimiii/guildion-signaling/config/development"
	"github.com/Zakimiii/guildion-signaling/config/production"
)

type ENV struct {
	Singleton
	API_KEY          string `json:"API_KEY"`
	API_SECRET       string `json:"API_SECRET"`
	SIGNALING_KEY    string `json:"SIGNALING_KEY"`
	SIGNALING_SECRET string `json:"SIGNALING_SECRET"`
	MYSQL_USERNAME   string `json:"MYSQL_USERNAME"`
	MYSQL_PASSWORD   string `json:"MYSQL_PASSWORD"`
	MYSQL_DATABASE   string `json:"MYSQL_DATABASE"`
	MYSQL_HOST       string `json:"MYSQL_HOST"`
	MYSQL_PROTOCOL   string `json:"MYSQL_PROTOCOL"`
	MYSQL_DIALECT    string `json:"MYSQL_DIALECT"`
}

var instance *ENV

func GetENV() *ENV {
	if instance == nil {
		mode := os.Getenv("BUILD_MODE")
		if mode == "" {
			mode = "development"
		}
		switch mode {
		case "development":
			instance = &ENV{
				API_KEY:          development.API_KEY,
				API_SECRET:       development.API_SECRET,
				SIGNALING_KEY:    development.SIGNALING_KEY,
				SIGNALING_SECRET: development.SIGNALING_SECRET,
				MYSQL_USERNAME:   development.MYSQL_USERNAME,
				MYSQL_PASSWORD:   development.MYSQL_PASSWORD,
				MYSQL_DATABASE:   development.MYSQL_DATABASE,
				MYSQL_HOST:       development.MYSQL_HOST,
				MYSQL_PROTOCOL:   development.MYSQL_PROTOCOL,
				MYSQL_DIALECT:    development.MYSQL_DIALECT,
			}
		case "production":
			instance = &ENV{
				API_KEY:          production.API_KEY,
				API_SECRET:       production.API_SECRET,
				SIGNALING_KEY:    production.SIGNALING_KEY,
				SIGNALING_SECRET: production.SIGNALING_SECRET,
				MYSQL_USERNAME:   production.MYSQL_USERNAME,
				MYSQL_PASSWORD:   production.MYSQL_PASSWORD,
				MYSQL_DATABASE:   production.MYSQL_DATABASE,
				MYSQL_HOST:       production.MYSQL_HOST,
				MYSQL_PROTOCOL:   production.MYSQL_PROTOCOL,
				MYSQL_DIALECT:    production.MYSQL_DIALECT,
			}
		default:
			instance = &ENV{
				API_KEY:          development.API_KEY,
				API_SECRET:       development.API_SECRET,
				SIGNALING_KEY:    development.SIGNALING_KEY,
				SIGNALING_SECRET: development.SIGNALING_SECRET,
				MYSQL_USERNAME:   development.MYSQL_USERNAME,
				MYSQL_PASSWORD:   development.MYSQL_PASSWORD,
				MYSQL_DATABASE:   development.MYSQL_DATABASE,
				MYSQL_HOST:       development.MYSQL_HOST,
				MYSQL_PROTOCOL:   development.MYSQL_PROTOCOL,
				MYSQL_DIALECT:    development.MYSQL_DIALECT,
			}
		}
	}
	return instance
}
