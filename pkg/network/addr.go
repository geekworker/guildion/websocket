package network

import (
  "log"
  "errors"
  "os"
)

func GetLocalHost() string {
  mode := os.Getenv("BUILD_MODE")
  domain := os.Getenv("DOMAIN")
  if mode == "" {
    mode = "development"
  }

  if domain == "" && mode != "development" {
    log.Fatal(errors.New("Missing $DOMAIN"))
  } else if domain == "" && mode == "development" {
    port := os.Getenv("PORT")
    domain = "localhost:" + port
  }
  return domain
}
