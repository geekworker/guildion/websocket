package network

import (
  "github.com/thoas/go-funk"

  "github.com/Zakimiii/guildion-signaling/pkg/domain/model"
)

type ConnectingClients struct {
  Singleton
  items []*model.ClientModel
}

var connectingClients *ConnectingClients

func GetConnectingClients() *ConnectingClients {
  if connectingClients == nil {
    connectingClients = &ConnectingClients{
      items: make([]*model.ClientModel, 0),
    }
  }
  return connectingClients
}

func GetClients() []*model.ClientModel {
  return GetConnectingClients().items
}


func AppendClients(client *model.ClientModel) {
  GetConnectingClients().items = append(GetConnectingClients().items, client)
}

func RemoveClients() {
  GetConnectingClients().items = funk.Filter(GetConnectingClients().items, func(model *model.ClientModel) bool { return model.Connecting }).([]*model.ClientModel)
}
