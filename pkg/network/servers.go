package network

import (
  "github.com/Zakimiii/guildion-signaling/pkg/data/entity"
)

type ConnectingServers struct {
  Singleton
  items []*entity.ServerEntity
  current *entity.ServerEntity
}

var connectingServers *ConnectingServers

func GetConnectingServers() *ConnectingServers {
  if connectingServers == nil {
    connectingServers = &ConnectingServers{
      items: make([]*entity.ServerEntity, 0),
      current: &entity.ServerEntity{},
    }
  }
  return connectingServers
}

func GetServers() []*entity.ServerEntity {
  return GetConnectingServers().items
}

func GetCurrentServer() *entity.ServerEntity {
  return GetConnectingServers().current
}
