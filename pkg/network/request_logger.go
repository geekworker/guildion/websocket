package network

import (
  "time"
  "log"

  "strconv"
)

type RequestLogger struct {
  Path      string
  Action      string
  StartedAt time.Time
  EndAt     time.Time
}

func (logger RequestLogger) Start() {
  logger.StartedAt = time.Now()
}

func (logger RequestLogger) End() {
  logger.EndAt = time.Now()
  log.Println("debug:", "[" + logger.Action + "]", logger.Path, ",", logger.RequestTotalMS(), "\n")
}

func (logger RequestLogger) RequestTotalMS() string {
  ms := float64(float64(logger.EndAt.Sub(logger.StartedAt)) / float64(time.Millisecond) / 1000000000000)
  return strconv.FormatFloat(ms, 'f', 4, 64) + "ms"
}
