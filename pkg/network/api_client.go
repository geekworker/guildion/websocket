package network

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
)

func PostAPIRequest(path string) string {
	args := url.Values{}
	env := GetENV()
	mode := os.Getenv("BUILD_MODE")
	args.Add("API_KEY", env.API_KEY)
	args.Add("API_SECRET", env.API_SECRET)
	url_target := ""
	switch mode {
	case "development":
		url_target = "http://localhost8080" + path
	case "production":
		url_target = "https://guildion.us" + path
	default:
		url_target = "https://guildion.us" + path
	}
	res, err := http.PostForm(url_target, args)
	if err != nil {
		fmt.Println("Request error:", err)
		return ""
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println("Request error:", err)
		return ""
	}
	fmt.Println("[body] " + string(body))
	return string(body)
}
