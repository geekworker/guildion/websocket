package router

import (
  "flag"
  "log"
  "net/http"
  "os"

  "github.com/comail/colog"
  "github.com/gorilla/mux"

  "github.com/Zakimiii/guildion-signaling/pkg/adapter/middleware"
  "github.com/Zakimiii/guildion-signaling/pkg/external/mysql"
  _ "github.com/Zakimiii/guildion-signaling/pkg/network"
)

func Run() {
  colog.SetMinLevel(colog.LTrace)
  colog.SetDefaultLevel(colog.LDebug)
  colog.ParseFields(true)
  colog.Register()
  port := os.Getenv("PORT")
  mode := os.Getenv("BUILD_MODE")

  if mode == "" {
    mode = "development"
  }

  if port == "" {
    log.Fatal("$PORT must be set")
  }
  var addr = flag.String("addr", ":" + port, "The listen port of the application.")
  flag.Parse()

  router := mux.NewRouter()
  mysql.Connect()
  router.HandleFunc("/signaling/v1/", middleware.SignalingConnectHandler)
  router.HandleFunc("/api/v1/room/{name}/members/count", middleware.RoomMembersCountHandler).Methods("GET")
  router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Welcome to Guildion Websocket API\n"))
  })

  http.Handle("/", router)
  log.Println("info: * Environment:", mode)
  log.Println("info: * Starting web server on", *addr)

  middleware.ScaleConstructHandler()

  if err := http.ListenAndServe(*addr, nil); err != nil {
    log.Fatal("ListenAndServe:", err)
  }

  defer func() {
    middleware.ScaleDestructHandler()
  }()
}
