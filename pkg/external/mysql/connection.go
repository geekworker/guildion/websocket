package mysql

import (
  "os"
  "log"
  "path/filepath"
  "fmt"
  "runtime"

  "github.com/jinzhu/gorm"
  _ "github.com/jinzhu/gorm/dialects/mysql"
  "github.com/qor/validations"
  "github.com/pressly/goose"

  "github.com/Zakimiii/guildion-signaling/pkg/network"
)

type MysqlConnector struct {
  Dialect  string `json:"dialect"`
  Username string `json:"username"`
  Password string `json:"password"`
  Protocol string `json:"protocol"`
  Database string `json:"database"`
}

var DB *gorm.DB

func getConnector() MysqlConnector {
  var connector = MysqlConnector{
    Dialect: network.GetENV().MYSQL_DIALECT,
    Username: network.GetENV().MYSQL_USERNAME,
    Password: network.GetENV().MYSQL_PASSWORD,
    Protocol: network.GetENV().MYSQL_PROTOCOL,
    Database: network.GetENV().MYSQL_DATABASE,
  }
  return connector
}

func Connect() *gorm.DB {
  var err error
  mode := os.Getenv("BUILD_MODE")
  db_path := os.Getenv("DB_PATH")
  if mode == "" {
    mode = "development"
  }

  var (
    _, b, _, _ = runtime.Caller(0)
    Root = filepath.Join(filepath.Dir(b), "../../../db")
  )

  connector := getConnector()
  connectTemplate := "%s:%s@%s/%s"
  connect := fmt.Sprintf(connectTemplate, connector.Username, connector.Password, connector.Protocol, connector.Database)
  db, err := gorm.Open(connector.Dialect, connect)
  _db, _ := goose.OpenDBWithDriver(connector.Dialect, connect)

  if db_path != "" {
    Root = db_path
  }

  if __err := goose.Run("up", _db, filepath.Join(Root, "migrations")); __err != nil {
    log.Fatalf("goose migrations", "up", __err)
  }
  DB = db
  validations.RegisterCallbacks(DB)

  if err != nil {
    panic(err)
  }
  return DB
}

func CloseConn() {
  DB.Close()
}
