FROM golang:1.14 AS build-env

MAINTAINER zakimiii

COPY . /src

WORKDIR /src/cmd/guildion-signaling

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -a -installsuffix cgo -o guildion-signaling \
    && mv ./guildion-signaling /

FROM scratch

COPY --from=build-env /guildion-signaling /
COPY --from=build-env /src/db/ /db/

ENV DB_PATH db

EXPOSE 8080

CMD ["./guildion-signaling"]
