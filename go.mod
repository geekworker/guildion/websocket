module github.com/Zakimiii/guildion-signaling

go 1.14

require (
	github.com/asaskevich/govalidator v0.0.0-20200907205600-7a23bdc65eef // indirect
	github.com/comail/colog v0.0.0-20160416085026-fba8e7b1f46c
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pressly/goose v2.7.0+incompatible
	github.com/qor/qor v0.0.0-20200729071734-d587cffbbb93 // indirect
	github.com/qor/validations v0.0.0-20171228122639-f364bca61b46
	github.com/thoas/go-funk v0.7.0
)
