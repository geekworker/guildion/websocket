-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE servers (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(126) NOT NULL,
  `created_at` timestamp NULL default now(),
  `updated_at` timestamp NULL default now(),
  PRIMARY KEY(id),
  UNIQUE KEY `host` (`host`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

CREATE TABLE clients (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `connection_id` varchar(126) NOT NULL,
  `sec_websocket_key` varchar(255) NOT NULL,
  `server_id` INT(11) NOT NULL,
  `connecting` tinyint(1) NOT NULL,
  `created_at` timestamp NULL default now(),
  `updated_at` timestamp NULL default now(),
  PRIMARY KEY(id),
  KEY `server_id` (`server_id`),
  UNIQUE KEY `connection_id` (`connection_id`),
  CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`server_id`) REFERENCES `servers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

CREATE TABLE rooms (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(126) NOT NULL,
  `created_at` timestamp NULL default now(),
  `updated_at` timestamp NULL default now(),
  PRIMARY KEY(id),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

CREATE TABLE members (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `client_id` INT(11) NOT NULL,
  `room_id` INT(11) NOT NULL,
  `created_at` timestamp NULL default now(),
  PRIMARY KEY(id),
  KEY `client_id` (`client_id`),
  KEY `room_id` (`room_id`),
  CONSTRAINT `members_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `members_ibfk_2` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

CREATE TABLE messages (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `member_id` INT(11) NOT NULL,
  `data` longtext,
  `created_at` timestamp NULL default now(),
  `updated_at` timestamp NULL default now(),
  PRIMARY KEY(id),
  KEY `member_id` (`member_id`),
  CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.
DROP TABLE messages;
DROP TABLE members;
DROP TABLE clients;
DROP TABLE rooms;
DROP TABLE servers;
