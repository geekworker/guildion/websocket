package main

import (
  "github.com/Zakimiii/guildion-signaling/pkg/external/router"
  "github.com/Zakimiii/guildion-signaling/pkg/external/mysql"
)

func main() {
  defer mysql.CloseConn()
  router.Run()
}
