# Guildion Signaling Server

https://signaling.guildion.us

The websocket signaling server for Guildion that community building service for movie watchers ( https://guildion.us )

## Installation

#### Docker Compose

```
docker-compose up -d
```

#### Build Docker

````
git clone https://gitlab.com/Zakimiii/guildion-signaling
cd guildion-signaling
docker build . -t guildion:guldion-signaling:[tagname]
````

## Useage

#### To run Guildion Signaling in development mode

```
PORT=1000 go run cmd/guildion-signaling/main.go
```